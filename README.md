# Project 10-Stratego

![logo](https://i.ibb.co/YWBjnGw/Stratego.png)

## About the game 
Stratego is a strategy board game for two players on a board of 10×10 squares. Each player controls 40 pieces representing individual officer and soldier ranks in an army. The objective of the game is to find and capture the opponent's Flag.

### Gameplay
Players alternate moving. 

- ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) The average game has **381** moves. 
- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) The number of legal positions is **10<sup>15</sup>**.


- ![#1589F0](https://via.placeholder.com/15/1589F0/000000?text=+) The number of possible games is **10<sup>535</sup>**.

 Stratego has many more moves and substantially *greater complexity* than other familiar games such as chess and backgammon.

### Pieces


|Rank|Piece|No. per player|Special properties|
|----|-----|----------------|------------------|
|B|Bomb|6|Immovable; is captured by Miner|
10/1|Marshal|1|Can be captured by the Spy if the Spy attacks first.
9/2|General|1|
8/3|Colonel|2|
7/4|Major|3|
6/5|Captain|4|
5/6|Lieutenant|4|
4/7|Sergeant|4|
3/8|Miner|5|Can defuse (i. e. capture) bombs
2/9|Scout|8|moves any distance in a horizontal or vertical straight line, without leaping over pieces/lakes
1/S|Spy|1|Defeats the Marshal, but only if the Spy makes the attack
F|Flag|1|Immovable; capturing the opponent's Flag wins the game

## Quick look
![quick look](https://i.ibb.co/8jjWyvq/screen-Shot.png)

## Demo
[Tutorial Video](https://photos.google.com/share/AF1QipPUmivLsrQLv4Rs-xS6NmldmdFN97nDYohiZgxAVktdjsYLYH831unbOR5PGtV7VA/photo/AF1QipPDZ3sc6UuaQ6g12DKc3gH4DlnD-rRmPu5RnwL_?key=SUppOEVkaFQ3bFpFdWhHNVE4RXBYM0FtUWV2Z0Zn)

### Enjoy!

## Developers

- [Marija Radović, 269/2017](https://gitlab.com/MarijaRadovic)
- [Nikola Jovanović, 78/2017](https://gitlab.com/LuciusVorenus98)
- [Jovan Stamenković, 148/2017](https://gitlab.com/JovanS98)
- [Tamara Jevtimijević, 261/2017](https://gitlab.com/tamaricajev)
