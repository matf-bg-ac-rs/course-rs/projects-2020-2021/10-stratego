﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "tablefigurinescene.h"
#include "board.h"
#include "field.h"
#include "figurine.h"
#include "player.h"
#include "game.h"
#include "musicthread.h"
#include "tablesetupscene.h"
#include <QMainWindow>
#include <QMediaPlayer>
#include <QGridLayout>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QLabel>
#include <QPushButton>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QMessageBox>
#include <QListWidget>
#include <QListWidgetItem>
#include <iostream>
#include <QPainter>
#include <set>
#include <QColor>
#include <time.h>
#include <stdlib.h>

class TableFigurineScene;
class TableSetupScene;

namespace Ui {
class MainWindow;
}
typedef std::vector<std::vector<Field>> State;
typedef std::vector<std::vector<Field*>>* pState;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void back_clicked();
    void newgame_clicked();
    void continuegame_clicked();
    void options_clicked();
    void exit_clicked();

    void options_rules_clicked();
    void rules_back_clicked();
    void setup_done_clicked();
    void setup_back_clicked();
    void main_options_clicked();
    void main_quit_clicked();
    int volume() const;
    void setVolume(int volume);
    void on_pbOnOff_toggled(bool checked);
    void on_pbExit_pressed();
    void on_pbOptions_pressed();
    void on_pbContinue_pressed();
    void on_pbNewGame_pressed();

    void on_pbMPback_pressed();
    void on_pbOptionsRules_pressed();
    void on_pbOptionsBack_pressed();
    void on_pbRulesBack_pressed();
    void on_pbSetupAutoCompletePressed();
    void on_pbSetupDonePressed();
    void on_pbSetupBackPressed();
    void on_pbMainOptionsPressed();
    void on_pbMainQuitPressed();
    void on_rbEasy_pressed();
    void on_rbMedium_pressed();
    void on_rbHard_pressed();

private:
    Ui::MainWindow *ui;
    QMediaPlayer* player;
    QMediaPlayer* medium_game_song;
    QMediaPlayer* easy_game_song;
    QMediaPlayer* hard_game_song;
    QMediaPlayer* buttons_click2;
    QMediaPlayer* on_off_click;
    QMediaPlayer* radio_button_click;
    QMediaPlaylist* playlistsound;
    QMediaPlaylist* playlistmedium;
    QMediaPlaylist* playlisteasy;
    QMediaPlaylist* playlisthard;
    QGridLayout *setupLayout;
    QGraphicsView *setupViewTable;
    QGraphicsView *setupViewFigurines;
    TableSetupScene *setupSceneTable;
    TableFigurineScene *setupSceneFigurine;
    QPushButton *pbSetupAutoComplete;
    QPushButton *pbSetupDone;
    QPushButton *pbSetupBack;
    QGridLayout *mainLayout;
    QGraphicsView *mainView;
    QPushButton *pbMainOptions;
    QPushButton *pbMainQuit;

    Game* game;

    int backIndex;
    int volumeValue;
    GameScene *gameScene;
    Board* playingBoard;

    QLabel *notification;

    int m_computerPresetIndex;

    GameDifficulty selectedDifficulty;
    bool m_gameStarted = false;


private:
    std::vector<std::vector<Field *>> *findPreset(int index);
    void putFigurineInPreset(int i, int j, std::set<Figurine*> *figurines,
                             std::vector<std::vector<Field *>> *currentPreset,
                             FigurineType ft);
    void initializeSetOfFigurines(std::set<Figurine*> *figurines);
    void initializeMusic();
    void initializeSetupSceneButtons();
    void initializeMainSceneButtons();
    void loadPresets();
    void initializeSetupScenes();
    void initializeNewGame();
    void initializeGameScene();
    void initializeSetupLayout();
    void initializeMainLayout();
    void connectSignalsAndSlots();
    void initializeNotification();
    void initializeListRules();
    QPushButton* initializeButton(QString text);
    void musicOnOff(bool on);
    void musicMuteUnmute(bool shouldPlay);
    void setProbability();


signals:
    void onMuteMusic();
    void onUnmuteMusic();
    void onStopMusic();
    void onStartMusic();
};

#endif // MAINWINDOW_H
