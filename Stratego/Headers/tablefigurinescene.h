#ifndef TABLEFIGURINE_H
#define TABLEFIGURINE_H

#include <QGraphicsScene>
#include <QPoint>
#include <QMouseEvent>
#include <QGraphicsView>
#include <vector>
#include "figurine.h"
#include "tablesetupscene.h"
#include "field.h"
#include "mainwindow.h"
#include "board.h"

class TableSetupScene;

class TableFigurineScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit TableFigurineScene(QObject *parent = 0);
    ~TableFigurineScene();
    void setOtherScene(TableSetupScene*);
    void setView(QGraphicsView*);
    void SetClickedFigurine(Figurine*);
    Figurine* getClickedFigurine();
    Board *getBoard();
    void setBoard(Board* board);
    SelectingStatus status;

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);

private:
    Figurine *m_clickedFigurine;
    TableSetupScene *m_otherScene;
    QGraphicsView *m_view;
    Board* m_board;

};

#endif // TABLEFIGURINE_H
